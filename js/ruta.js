document.addEventListener('DOMContentLoaded', function () {
    var map = L.map('map').setView([23.238488155275792, -106.36991484699881], 13);
    var userMarker;
    var defaultSpeed = 30; // Velocidad promedio predeterminada (en km/h)

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    // Obtener la ubicación actual del usuario
    if ("geolocation" in navigator) {
        navigator.geolocation.watchPosition(function(position) {
            var lat = position.coords.latitude;
            var lon = position.coords.longitude;
            
            var currentLocationIcon = L.divIcon({
                className: 'current-location-icon',
                html: '<div class="circle"></div>'
            });
            userMarker = L.marker([lat, lon], { icon: currentLocationIcon }).addTo(map);
            map.setView([lat, lon], 13);
            
            // Mostrar las coordenadas actuales en el label
            document.getElementById("current-coordinates").textContent = "Ubicación Actual: " + lat.toFixed(6) + ", " + lon.toFixed(6);
            
            // Calcular y mostrar la hora estimada de llegada al destino
            if (currentRoute) {
                // Calcular la distancia al destino
                var destination = currentRoute.getWaypoints()[1].latLng;
                var distanceToDestination = userMarker.getLatLng().distanceTo(destination) / 1000; // Convertir a kilómetros
                
                // Calcular y mostrar la hora estimada de llegada
                var estimatedTimeDefaultSpeed = calculateEstimatedArrivalTime(defaultSpeed, distanceToDestination); // Calcular con la velocidad predeterminada
                
                document.getElementById("estimated-arrival-time").textContent = "Tiempo Estimado de Llegada (60 km/h): " + estimatedTimeDefaultSpeed;
            }
        }, function(error) {
            console.error('Error al obtener la ubicación: ', error);
        });
    } else {
        console.error('Geolocalización no es compatible en este navegador.');
    }

    // Configurar marcadores de destino
    var destinations = [
        { name: 'Destino 1', coordinates: [23.238488155275792, -106.36991484699881] },
        { name: 'Destino 2', coordinates: [23.21201372173557, -106.42122126554736] },
        { name: 'Destino 3', coordinates: [23.268700114647356, -106.42774800324672] },
        { name: 'Destino 4', coordinates: [23.233651369773085, -106.42388628669367] },
        { name: 'Destino 5', coordinates: [23.254774085423072, -106.41910620652875] },
        { name: 'Destino 6', coordinates: [23.239831228757144, -106.43034573165615] },
        { name: 'Destino 7', coordinates: [23.24602953624312, -106.38768654699858] }
    ];

    var customIcon = L.icon({
        iconUrl: '/img/ubicacion.png',
        iconSize: [32, 32],
        iconAnchor: [16, 32],
        popupAnchor: [0, -32]
    });

    // Agregar marcadores de destino
    destinations.forEach(destination => {
        L.marker(destination.coordinates, { icon: customIcon }).addTo(map).bindPopup(destination.name).on('click', function(e) {
            calculateRoute(e.latlng); // Calcular ruta cuando se hace clic en un marcador de destino
        });
    });

   // Variable para almacenar la referencia a la ruta actual
var currentRoute;

// Función para calcular la ruta utilizando Leaflet Routing Machine

function calculateRoute(destination) {
    if (userMarker) {
        var userLocation = userMarker.getLatLng();
        var start = userLocation;
        var end = destination;

        // Calcular la distancia al destino
        var distanceToDestination = userLocation.distanceTo(destination) / 1000; // Convertir a kilómetros

        // Eliminar la ruta anterior si existe
        if (currentRoute) {
            map.removeControl(currentRoute);
            currentRoute = null; // Asignar null a currentRoute para indicar que no hay ruta actual
        }

        // Calcular la nueva ruta
        currentRoute = L.Routing.control({
            waypoints: [
                L.latLng(start.lat, start.lng),
                L.latLng(end.lat, end.lng)
            ],
            routeWhileDragging: true,
            createMarker: function (i, waypoint, n) {
                // No crear marcadores
                return null;
            }
        }).addTo(map);

        // Calcular y mostrar la hora estimada de llegada al destino
        var estimatedTimeDefaultSpeed = calculateEstimatedArrivalTime(defaultSpeed, distanceToDestination); // Calcular con la velocidad predeterminada

        document.getElementById("estimated-arrival-time").textContent = "Tiempo Estimado de Llegada (60 km/h): " + estimatedTimeDefaultSpeed;

    } else {
        alert("No se puede calcular la ruta porque no se ha podido obtener la ubicación del usuario.");
    }
}

// Función para calcular la hora estimada de llegada
function calculateEstimatedArrivalTime(speed, distance) {
    // Calcular el tiempo estimado de viaje en minutos (tiempo = distancia / velocidad * 60)
    var travelTimeMinutes = distance / speed * 60;

    // Redondear el tiempo estimado de viaje a minutos enteros
    var roundedTravelTimeMinutes = Math.round(travelTimeMinutes);

    // Convertir los minutos a horas y minutos
    var hours = Math.floor(roundedTravelTimeMinutes / 60);
    var minutes = roundedTravelTimeMinutes % 60;

    // Crear un string con el tiempo estimado en un formato legible
    var estimatedTime = "";

    if (hours > 0) {
        estimatedTime += hours + " hora" + (hours > 1 ? "s" : "");
        if (minutes > 0) {
            estimatedTime += " y ";
        }
    }

    if (minutes > 0) {
        estimatedTime += minutes + " minuto" + (minutes > 1 ? "s" : "");
    }

    return estimatedTime;
}



});
